﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Collections.ObjectModel;
using System.Globalization;
using RestSharp;
using Microsoft.Phone.Net.NetworkInformation;
using Newtonsoft.Json;
using Telerik.Windows.Controls;
using Microsoft.Phone.Tasks;

namespace DN_Reader
{
    public partial class PivotPage1 : PhoneApplicationPage
    {
        private ObservableCollection<Story> topStories, newStories;
        public PivotPage1()
        {
            InitializeComponent();
            SystemTray.SetOpacity(this, 0.01);
            Loaded += (s, e) =>
            {
                InteractionEffectManager.AllowedTypes.Add(typeof(StackPanel));
                if (CheckInternetConnection())
                {
                    loadTopStories();
                    loadRecentStories();
                }
                else
                {
                    System.Diagnostics.Debug.WriteLine("Halo!");
                    showInternetUnavailable();
                }
            };
        }

        private void showInternetUnavailable()
        {
            System.Diagnostics.Debug.WriteLine("internet unavailable");
            
            MessageBox.Show("Sorry, your internet connection is unavailable or too slow", "Information", MessageBoxButton.OK);
 
        }

        private void loadTopStories()
        {
            var dnApi = new DNApi();
            var client = dnApi.getClient();
            var request = dnApi.getTopStories();
            pg.IsIndeterminate = true;
            System.Diagnostics.Debug.WriteLine("mula execute");
            try
            {
                var asynchandle = client.ExecuteAsync(request, response =>
                {
                    System.Diagnostics.Debug.WriteLine("sakses!");
                    System.Diagnostics.Debug.WriteLine(response.Content);

                    RootStory json = JsonConvert.DeserializeObject<RootStory>(response.Content);
                    topStories = new ObservableCollection<Story>();
                    foreach (var item in json.stories)
                    {
                        topStories.Add(item);
                    }

                    pg.IsIndeterminate = false;
                    this.topStory.ItemsSource = topStories;
                });
            }
            catch (Exception)
            {
                MessageBox.Show("No connection available", "Warning", MessageBoxButton.OK);
            }
        }

        private void loadRecentStories()
        {
            var dnApi = new DNApi();
            var client = dnApi.getClient();
            var request = dnApi.getRecentStories();
            pg.IsIndeterminate = true;
            System.Diagnostics.Debug.WriteLine("mula execute");
            try
            {
                var asynchandle = client.ExecuteAsync(request, response =>
                {
                    System.Diagnostics.Debug.WriteLine("sakses!");
                    System.Diagnostics.Debug.WriteLine(response.Content);

                    RootStory json = JsonConvert.DeserializeObject<RootStory>(response.Content);
                    newStories = new ObservableCollection<Story>();
                    foreach (var item in json.stories)
                    {
                        newStories.Add(item);
                    }

                    pg.IsIndeterminate = false;
                    this.recentStory.ItemsSource = newStories;
                });
            }
            catch (Exception)
            {
                MessageBox.Show("No connection available", "Warning", MessageBoxButton.OK);
            }
            
        }

        public static bool CheckInternetConnection()
        {
            bool isConnected = true;

            if (NetworkInterface.NetworkInterfaceType == NetworkInterfaceType.None)
            {
                isConnected = false;
            }

            
            return isConnected;
        }

        private void topStory_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Story story = (Story)topStory.SelectedItem;
            WebBrowserTask web = new WebBrowserTask();
            web.Uri = new Uri("http://www.instapaper.com/text?u=" + story.url);
            web.Show();
        }

        private void refresh_news(object sender, EventArgs e)
        {
            if (CheckInternetConnection())
            {
                loadRecentStories();
                loadTopStories();
            }
            else
            {
                showInternetUnavailable();
            }
        }

        private void about_click(object sender, EventArgs e)
        {
            Dispatcher.BeginInvoke(() =>
            {
                this.NavigationService.Navigate(new Uri("/About.xaml", UriKind.RelativeOrAbsolute));
            });
        }

        private void review_click(object sender, EventArgs e)
        {
            MarketplaceReviewTask marketplaceReviewTask = new MarketplaceReviewTask();

            marketplaceReviewTask.Show();
        }

        private void settings_click(object sender, EventArgs e)
        {
            Dispatcher.BeginInvoke(() =>
            {
                this.NavigationService.Navigate(new Uri("/Settings.xaml", UriKind.RelativeOrAbsolute));
            });
        }

        private void recentStory_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Story story = (Story)recentStory.SelectedItem;
            WebBrowserTask web = new WebBrowserTask();
            web.Uri = new Uri("http://www.instapaper.com/text?u=" + story.url);
            web.Show();
        }

    }

}