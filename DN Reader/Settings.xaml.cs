﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Microsoft.Phone.Tasks;

namespace DN_Reader
{
    public partial class Settings : PhoneApplicationPage
    {
        public Settings()
        {
            InitializeComponent();
            SystemTray.SetOpacity(this, 0.01);

            List<string> _viewer = new List<string>();
            _viewer.Add("Default");
            _viewer.Add("Instapaper");
            viewer.ItemsSource = _viewer;
        }

        private void view_with_changed(object sender, SelectionChangedEventArgs e)
        {
            showComingSoonMessage();
        }

        private void ToggleSwitch_Checked(object sender, RoutedEventArgs e)
        {
            showComingSoonMessage();
        }

        private void showComingSoonMessage()
        {

            //MessageBox.Show("This feature will be availble soon, wait for another update", "Information", MessageBoxButton.OK);

            //CustomMessageBox cmb = new CustomMessageBox()
            //{
            //    Content = "This feature will be availble soon, wait for another update",
            //    Title = "Information",
            //    IsFullScreen = false,
            //    IsLeftButtonEnabled = false,
            //    RightButtonContent = "It's fine",
            //};
            //cmb.Show();
        }

       
    }
}
