﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Reader
{
    class DNApi
    {
        private RestClient client = new RestClient("https://news.layervault.com");
        public DNApi()
        {

        }

        public RestClient getClient()
        {
            return this.client;
        }

        public RestRequest getTopStories()
        {
            RestRequest request = new RestRequest("stories.json", Method.GET);
            return request;
        }

        public RestRequest getRecentStories()
        {
            RestRequest request = new RestRequest("new.json", Method.GET);
            return request;
        }
    }
}
