﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN_Reader
{
    public class Story
    {
        public int id { get; set; }
        public string title { set; get; }
        public string comment
        {
            set;
            get;
        }
        public int vote_count { set; get; }
        public string created_at
        {
            get;
            set;
        }
        public object pinned_at { get; set; }
        public string url { get; set; }
        public int num_comments { get; set; }
        public string submitter_display_name { get; set; }
        private string _info;
        public string info
        {
            get
            {
                DateTime dt = DateTime.Parse(created_at);
                _info = vote_count.ToString() +" points and " + num_comments + " comments " + DateHelper.ToRelativeDate(dt);
                return _info;
            }
            set{
                _info = value;
            }

        }
        public string submitter
        {
            get
            {
                return "By " + submitter_display_name;
            }

            set
            {
                submitter_display_name = value;
            }
            
        }

    }
}
